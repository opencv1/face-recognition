#include "Dataset.h"

Dataset::Dataset(){

}

void Dataset::setDirDataset(string dir){
	dirDataset = dir;
}

string Dataset::getDirDataset(){
	return dirDataset;
}

void Dataset::setImageExtension(string ext){
	imageExtension = ext;
}

string Dataset::getImageExtension(){
	return imageExtension;
}

int isImageFile(string f, string ext){
    if(strstr(f.c_str(), ext.c_str())){
        return 1;
    }
    return 0;
}

void Dataset::setListImages(string dir){
	DIR *dp;
    struct dirent *ep;

    dp = opendir (dir.c_str());
    if (dp != NULL){
        while ((ep = readdir (dp))){
            if(isImageFile(ep->d_name, imageExtension)){
                listImages.push_back(ep->d_name);
            }
        }
        (void) closedir (dp);
    }else{
        perror ("Couldn't open the directory");
    }
}

vector<string> Dataset::getListImages(){
	return listImages;
}