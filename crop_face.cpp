#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>

#include "Dataset.h"

using namespace std;
using namespace cv;


string face_cascade_name = "haarcascade_frontalface_alt.xml";
CascadeClassifier face_cascade;
string filename;
int filenameNumber = 0;

void detectAndCropFace(Mat frame);

int main(int argc, char const *argv[]){
    // Load the cascade
    if (!face_cascade.load(face_cascade_name)){
        printf("Error loading\n");
        return (-1);
    }

    Dataset data;
    string dir = argv[1], ext = ".jpg";
    data.setDirDataset(dir);
    data.setImageExtension(ext);
    data.setListImages(data.getDirDataset());
    vector<string> listImages = data.getListImages();

    int sizeDataset = listImages.size();
    for (int i = 0; i < sizeDataset; i++){
        char imageUrl[128];
        sprintf(imageUrl, "%s/%s", data.getDirDataset().c_str(), listImages.at(i).c_str());

        // Read the image file
        Mat frame = imread(imageUrl);

        // Apply the classifier to the frame
        if (!frame.empty()){
            detectAndCropFace(frame);
        }
        else{
            printf(" --(!) No captured frame -- Break!");
            return 1;
        }
    }

    return 0;
}

// Function detectAndDisplay
void detectAndCropFace(Mat frame)
{
    vector<Rect> faces;
    Mat frame_gray;
    Mat crop;
    Mat res;
    string text;
    stringstream sstm;

    cvtColor(frame, frame_gray, COLOR_BGR2GRAY);
    equalizeHist(frame_gray, frame_gray);

    // Detect faces
    face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30, 30));

    // Set Region of Interest
    Rect roi_c;

    for (int i = 0; i < faces.size(); i++) // Iterate through all current elements (detected faces)

    {
        roi_c.x = faces[i].x;
        roi_c.y = faces[i].y;
        roi_c.width = (faces[i].width);
        roi_c.height = (faces[i].height);

        crop = frame_gray(roi_c);
        resize(crop, res, Size(128, 128), 0, 0, INTER_LINEAR); // This will be needed later while saving images
        //cvtColor(res, gray, CV_BGR2GRAY); // Convert cropped image to Grayscale

        // Form a filename
        filename = "";
        stringstream ssfn;
        ssfn <<"crop_face/"<<filenameNumber << ".png";
        filename = ssfn.str();
        filenameNumber++;

        imwrite(filename, res);
    }
}
