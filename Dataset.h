#ifndef DATASET_H
#define DATASET_H

#include <vector>
#include <string>
#include <string.h>
#include <dirent.h>

using namespace std;

class Dataset
{
	public:
		Dataset();
		//~Dataset();
		
		string getDirDataset();
		void setDirDataset(string dirDataset);

		string getImageExtension();
		void setImageExtension(string imageExtension);

		vector<string> getListImages();
		void setListImages(string dir);
	private:
		string dirDataset;
		string imageExtension;
		vector<string> listImages;
};

#endif