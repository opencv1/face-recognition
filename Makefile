CFLAGS = `pkg-config --cflags opencv` -pthread -Wall -g -std=c++11
LDFLAGS = `pkg-config --libs opencv` -pthread 
LIBS = `pkg-config --libs opencv` -lm
CC = g++
OBJ1 = crop_face.o Dataset.o
OBJ2 = manual_classifier.o
OBJ3 = recognize.o
%.o: %.cpp 
		$(CC) -c -o $@ $< $(CFLAGS)
recognize: $(OBJ3)
	$(CC) -o $@ $^ $(LDFLAGS)	
crop_face_builder: $(OBJ1)
	$(CC) -o $@ $^ $(LDFLAGS)	
manual_classifier_builder: $(OBJ2)
	$(CC) -o $@ $^ $(LDFLAGS)
clean:
	rm -f *.o crop_face_builder Dataset manual_classifier recognize
