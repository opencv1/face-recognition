#include <stdio.h>		
#include <termios.h>	
#include <unistd.h>
#include <sys/types.h>
#include <iostream>
#include <sys/stat.h>	
#include <vector>
#include <string>
#include <opencv2/opencv.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "cv.h"
#include "cvaux.h"
#include "highgui.h"

using namespace std;
using namespace cv;

const char *faceCascadeFilename = "haarcascade_frontalface_alt.xml";

int SAVE_EIGENFACE_IMAGES 	  = 1;		// Set to 0 if you dont want images of the Eigenvectors saved to files (for debugging).

IplImage ** faceImgs          = 0; // array of face images
CvMat    *  personNumTruthMat = 0; // array of person numbers
vector<string> personNames;		   // array of person names (indexed by the person number). 

// Default dimensions for faces in the face recognition database. 
int faceWidth 				  = 128;
int faceHeight 				  = 128;

int numberTrainingPersons     = 0; 
int numberTrainingImages      = 0; 
int numberEigenValues         = 0; 

IplImage * avgTrainImage      = 0; 
IplImage ** eigenVectors      = 0; 
CvMat * eigenValueMat         = 0; 
CvMat * projectedTrainFaceMat = 0; 

float seuilConfidence;

string face_cascade_name      = "haarcascade_frontalface_alt.xml";
CascadeClassifier face_cascade;

void learn(const char *szFileTrain);
void processingPCA();
void storeTrainingData();
int loadTrainingData(CvMat ** pTrainPersonNumMat);
int findNearestNeighbor(float * projectedTestFace, float *probabilityConfidence);
int loadFaceImages(const char * filename);
void calculateMatrixConfusion(const char *szFileTest);
void recognize(Mat src);
IplImage* convertImageToGreyscale(const IplImage *imageSrc);
IplImage* cropImage(const IplImage *img, const CvRect region);
IplImage* resizeImage(const IplImage *origImg, int newWidth, int newHeight);
IplImage* convertFloatImageToUcharImage(const IplImage *srcImg);
void saveFloatImage(const char *filename, const IplImage *srcImg);
CvRect detectFaceInImage(const IplImage *inputImg, const CvHaarClassifierCascade* cascade );

int main( int argc, char** argv )
{
	if (!face_cascade.load(face_cascade_name)){
        printf("Error loading\n");
        return (-1);
    }
	if(argc >= 2 && strcmp(argv[1], "train") == 0){
		char *szFileTrain;
		if (argc == 3)
			szFileTrain = argv[2];	// use the given arg
		else {
			printf("ERROR: No training file given.\n");
			return 1;
		}
		learn(szFileTrain);
	}
	else if( argc >= 2 && strcmp(argv[1], "test") == 0){
		char *szFileTest;
		if (argc == 4){
			szFileTest = argv[2];	
			seuilConfidence = atof(argv[3]);
		}
		else {
			printf("ERROR: No testing file given.\n");
			return 1;
		}
		calculateMatrixConfusion(szFileTest);
	}else if( argc >= 2 && strcmp(argv[1], "recognize") == 0){
		string filename;
		if (argc == 4){
			filename = argv[2];	
			seuilConfidence = atof(argv[3]);
		}
		else {
			printf("ERROR: No image given.\n");
			return 1;
		}
		Mat src = imread(filename);
		recognize(src);
	}else{
		printf("Error command, use : ./recognize [train/test] [name file train/test/recognize] [seuilConfidence]");
	}
	return 0;
}

void storeEigenfaceImages()
{
	// Store the average image to a file
	printf("Saving the image of the average face as 'averageImage.png'.\n");
	cvSaveImage("averageImage.bmp", avgTrainImage);

	// Create a large image made of many eigenface images.
	// Must also convert each eigenface image to a normal 8-bit UCHAR image instead of a 32-bit float image.
	printf("Saving the %d eigenvector images as 'eigenumberFaces.png'\n", numberEigenValues);

	if(numberEigenValues > 0){
		// Put all the eigenumberFaces next to each other.
		int COLUMNS = 8;	// Put upto 8 images on a row.
		int numberCols = min(numberEigenValues, COLUMNS);
		int numberRows = 1 + (numberEigenValues / COLUMNS);	// Put the rest on new rows.
		int w = eigenVectors[0]->width;
		int h = eigenVectors[0]->height;
		CvSize size;
		size = cvSize(numberCols * w, numberRows * h);
		IplImage *bigImg = cvCreateImage(size, IPL_DEPTH_8U, 1);	
		for (int i = 0; i < numberEigenValues; i++) {
			// Get the eigenface image.
			IplImage *byteImg = convertFloatImageToUcharImage(eigenVectors[i]);
			// Paste it into the correct position.
			int x = w * (i % COLUMNS);
			int y = h * (i / COLUMNS);
			CvRect ROI = cvRect(x, y, w, h);
			cvSetImageROI(bigImg, ROI);
			cvCopyImage(byteImg, bigImg);
			cvResetImageROI(bigImg);
			cvReleaseImage(&byteImg);
		}
		cvSaveImage("eigenumberFaces.png", bigImg);
		cvReleaseImage(&bigImg);
	}
}

// Train from the data in the given text file, and store the trained data into the file 'facedata.xml'.
void learn(const char *szFileTrain)
{
	int offset;

	// load training data
	printf("Loading the training images in '%s'\n", szFileTrain);
	numberTrainingImages = loadFaceImages(szFileTrain);
	printf("Got %d training images.\n", numberTrainingImages);
	if( numberTrainingImages < 2 )
	{
		fprintf(stderr,
		        "Need 2 or more training faces\n"
		        "Input file contains only %d\n", numberTrainingImages);
		return;
	}

	// do PCA on the training faces
	processingPCA();

	// project the training images onto the PCA subspace
	projectedTrainFaceMat = cvCreateMat( numberTrainingImages, numberEigenValues, CV_32FC1 );
	offset = projectedTrainFaceMat->step / sizeof(float);
	for(int i = 0; i < numberTrainingImages; i++)
	{
		cvEigenDecomposite(
			faceImgs[i],
			numberEigenValues,
			eigenVectors,
			0, 0,
			avgTrainImage,
			projectedTrainFaceMat->data.fl + i * offset);
	}

	// store the recognition data as an xml file
	storeTrainingData();

	// Save all the eigenvectors as images, so that they can be checked.
	if (SAVE_EIGENFACE_IMAGES) {
		storeEigenfaceImages();
	}

}


// Open the training data from the file 'facedata.xml'.
int loadTrainingData(CvMat ** pTrainPersonNumMat)
{
	CvFileStorage * fileStorage;
	int i;

	// create a file-storage interface
	fileStorage = cvOpenFileStorage( "facedata.xml", 0, CV_STORAGE_READ );
	if( !fileStorage ) {
		printf("Can't open training database file 'facedata.xml'.\n");
		return 0;
	}

	// Load the person names. Added by Shervin.
	personNames.clear();	// Make sure it starts as empty.
	numberTrainingPersons = cvReadIntByName( fileStorage, 0, "numberTrainingPersons", 0 );
	if (numberTrainingPersons == 0) {
		printf("No people found in the training database 'facedata.xml'.\n");
		return 0;
	}
	// Load each person's name.
	for (i = 0; i < numberTrainingPersons; i++) {
		string sPersonName;
		char varname[200];
		snprintf( varname, sizeof(varname)-1, "personName_%d", (i+1) );
		sPersonName = cvReadStringByName(fileStorage, 0, varname );
		personNames.push_back( sPersonName );
	}

	// Load the data
	numberEigenValues = cvReadIntByName(fileStorage, 0, "numberEigenValues", 0);
	numberTrainingImages = cvReadIntByName(fileStorage, 0, "numberTrainingImages", 0);
	*pTrainPersonNumMat = (CvMat *)cvReadByName(fileStorage, 0, "trainPersonNumMat", 0);
	eigenValueMat  = (CvMat *)cvReadByName(fileStorage, 0, "eigenValueMat", 0);
	projectedTrainFaceMat = (CvMat *)cvReadByName(fileStorage, 0, "projectedTrainFaceMat", 0);
	avgTrainImage = (IplImage *)cvReadByName(fileStorage, 0, "avgTrainImg", 0);
	eigenVectors = (IplImage **)cvAlloc(numberTrainingImages*sizeof(IplImage *));
	for(i=0; i<numberEigenValues; i++){
		char varname[200];
		snprintf( varname, sizeof(varname)-1, "eigenVect_%d", i );
		eigenVectors[i] = (IplImage *)cvReadByName(fileStorage, 0, varname, 0);
	}

	// release the file-storage interface
	cvReleaseFileStorage( &fileStorage );

	printf("Training data loaded (%d training images of %d people):\n", numberTrainingImages, numberTrainingPersons);
	printf("People: ");
	if (numberTrainingPersons > 0)
		printf("<%s>", personNames[0].c_str());
	for (i=1; i<numberTrainingPersons; i++) {
		printf(", <%s>", personNames[i].c_str());
	}
	printf(".\n");

	return 1;
}


// Save the training data to the file 'facedata.xml'.
void storeTrainingData()
{
	CvFileStorage * fileStorage;
	int i;

	// create a file-storage interface
	fileStorage = cvOpenFileStorage( "facedata.xml", 0, CV_STORAGE_WRITE );

	// Store the person names. Added by Shervin.
	cvWriteInt( fileStorage, "numberTrainingPersons", numberTrainingPersons );
	for (i=0; i<numberTrainingPersons; i++) {
		char varname[200];
		snprintf( varname, sizeof(varname)-1, "personName_%d", (i+1) );
		cvWriteString(fileStorage, varname, personNames[i].c_str(), 0);
	}

	// store all the data
	cvWriteInt( fileStorage, "numberEigenValues", numberEigenValues );
	cvWriteInt( fileStorage, "numberTrainingImages", numberTrainingImages );
	cvWrite(fileStorage, "trainPersonNumMat", personNumTruthMat, cvAttrList(0,0));
	cvWrite(fileStorage, "eigenValueMat", eigenValueMat, cvAttrList(0,0));
	cvWrite(fileStorage, "projectedTrainFaceMat", projectedTrainFaceMat, cvAttrList(0,0));
	cvWrite(fileStorage, "avgTrainImg", avgTrainImage, cvAttrList(0,0));
	for(i=0; i<numberEigenValues; i++)
	{
		char varname[200];
		snprintf( varname, sizeof(varname)-1, "eigenVect_%d", i );
		cvWrite(fileStorage, varname, eigenVectors[i], cvAttrList(0,0));
	}

	// release the file-storage interface
	cvReleaseFileStorage( &fileStorage );
}

// Find the most likely person based on a detection. Returns the index, and stores the confidence value into probabilityConfidence.
int findNearestNeighbor(float * projectedTestFace, float *probabilityConfidence){
	double minDistance2 = DBL_MAX;
	cout<<minDistance2<<endl;
	int indexNearest = 0;

	for(int indexTrain = 0; indexTrain < numberTrainingImages; indexTrain++){
		double distance2 = 0;

		for(int i = 0; i < numberEigenValues; i++){
			float d_i = projectedTestFace[i] - projectedTrainFaceMat->data.fl[indexTrain * numberEigenValues + i];
			distance2 += d_i * d_i; 
		}

		if(distance2 < minDistance2)
		{
			minDistance2 = distance2;
			indexNearest = indexTrain;
		}
	}
	cout<<minDistance2<<endl;
	printf("=============\n");
	// Return the confidence level based on the Euclidean distance,
	// so that similar images should give a confidence between 0.5 to 1.0,
	// and very different images should give a confidence between 0.0 to 0.5.
	*probabilityConfidence = 1.0f - sqrt( minDistance2 / (float)(numberTrainingImages * numberEigenValues) ) / 255.0f;

	// Return the found index.
	return indexNearest;
}

// Do the Principal Component Analysis, finding the average image
// and the eigenumberFaces that represent any image in the given dataset.
void processingPCA()
{
	int i;
	CvTermCriteria calcLimit;
	CvSize faceImgSize;

	// set the number of eigenvalues to use
	numberEigenValues = numberTrainingImages-1;

	// allocate the eigenvector images
	faceImgSize.width  = faceImgs[0]->width;
	faceImgSize.height = faceImgs[0]->height;
	eigenVectors = (IplImage**)cvAlloc(sizeof(IplImage*) * numberEigenValues);
	for(i=0; i<numberEigenValues; i++)
		eigenVectors[i] = cvCreateImage(faceImgSize, IPL_DEPTH_32F, 1);

	// allocate the eigenvalue array
	eigenValueMat = cvCreateMat( 1, numberEigenValues, CV_32FC1 );

	// allocate the averaged image
	avgTrainImage = cvCreateImage(faceImgSize, IPL_DEPTH_32F, 1);

	// set the PCA termination criterion
	calcLimit = cvTermCriteria( CV_TERMCRIT_ITER, numberEigenValues, 1);

	// compute average image, eigenvalues, and eigenvectors
	cvCalcEigenObjects(
		numberTrainingImages,
		(void*)faceImgs,
		(void*)eigenVectors,
		CV_EIGOBJ_NO_CALLBACK,
		0,
		0,
		&calcLimit,
		avgTrainImage,
		eigenValueMat->data.fl);

	cvNormalize(eigenValueMat, eigenValueMat, 1, 0, CV_L1, 0);
}

// Read the names & image filenames of people from a text file, and load all those images listed.
int loadFaceImages(const char * filename)
{
	FILE * imgListFile = 0;
	char imgFilename[512];
	int numberFaces = 0;

	// open the input file
	if( !(imgListFile = fopen(filename, "r")) )
	{
		fprintf(stderr, "Can\'t open file %s\n", filename);
		return 0;
	}

	// count the number of faces
	while( fgets(imgFilename, sizeof(imgFilename)-1, imgListFile) ) ++numberFaces;
	rewind(imgListFile);

	// allocate the face-image array and person number matrix
	faceImgs = (IplImage **)cvAlloc( numberFaces*sizeof(IplImage *) );
	personNumTruthMat = cvCreateMat( 1, numberFaces, CV_32SC1 );

	personNames.clear();	// Make sure it starts as empty.
	numberTrainingPersons = 0;

	// store the face images in an array
	for(int indexFace=0; indexFace<numberFaces; indexFace++)
	{
		char personName[256];
		string sPersonName;
		int personNumber;

		// read person number (beginning with 1), their name and the image filename.
		fscanf(imgListFile, "%d %s %s", &personNumber, imgFilename, personName);
		sPersonName = personName;

		// Check if a new person is being loaded.
		if (personNumber > numberTrainingPersons) {
			// Allocate memory for the extra person (or possibly multiple), using this new person's name.
			for (int i = numberTrainingPersons; i < personNumber; i++) {
				personNames.push_back( sPersonName );
			}
			numberTrainingPersons = personNumber;
		}

		// Keep the data
		personNumTruthMat->data.i[indexFace] = personNumber;

		// load the face image
		faceImgs[indexFace] = cvLoadImage(imgFilename, CV_LOAD_IMAGE_GRAYSCALE);

		if( !faceImgs[indexFace] )
		{
			fprintf(stderr, "Can\'t load image from %s\n", imgFilename);
			return 0;
		}
	}

	fclose(imgListFile);

	printf("Data loaded from '%s': (%d images of %d people).\n", filename, numberFaces, numberTrainingPersons);
	printf("People: ");
	if (numberTrainingPersons > 0)
		printf("<%s>", personNames[0].c_str());
	for (int i = 1; i < numberTrainingPersons; i++) {
		printf(", <%s>", personNames[i].c_str());
	}
	printf(".\n");

	return numberFaces;
}


// Recognize the face in each of the test images given, and compare the results with the truth.
void calculateMatrixConfusion(const char *szFileTest)
{
	int nTestFaces  = 0;         // the number of test images
	CvMat * trainPersonNumMat = 0;  // the person numbers during training
	float * projectedTestFace = 0;
	const char *answer;
	int nCorrect = 0;
	int nWrong = 0;
	float confidence;

	// load test images and ground truth for person number
	nTestFaces = loadFaceImages(szFileTest);
	printf("%d test faces loaded\n", nTestFaces);

	int numberTestingPersons = numberTrainingPersons;
	int ** confusionMat = new int*[numberTestingPersons];

	for(int i = 0; i < numberTestingPersons; i++){
		confusionMat[i] = new int[numberTestingPersons];
		for(int j = 0; j < numberTestingPersons; j++){
			confusionMat[i][j] = 0;
		}
	}

	// load the saved training data
	if( !loadTrainingData( &trainPersonNumMat ) ) return;

	// project the test images onto the PCA subspace
	projectedTestFace = (float *)cvAlloc( numberEigenValues*sizeof(float) );
	
	for(int i = 0; i < nTestFaces; i++)
	{
		int indexNearest, nearest, truth;

		// project the test image onto the PCA subspace
		cvEigenDecomposite(
			faceImgs[i],
			numberEigenValues,
			eigenVectors,
			0, 0,
			avgTrainImage,
			projectedTestFace);

		indexNearest = findNearestNeighbor(projectedTestFace, &confidence);
		truth    = personNumTruthMat->data.i[i];
		if(confidence > seuilConfidence){
			nearest  = trainPersonNumMat->data.i[indexNearest];
		}else{
			nearest = numberTrainingPersons + 1;
		}

		confusionMat[truth - 1][nearest - 1]++;

		if (nearest == truth){
			answer = "Correct";
			nCorrect++;
		}
		else{
			answer = "WRONG!";
			nWrong++;
		}
		printf("nearest = %d, Truth = %d (%s). Confidence = %f\n", nearest, truth, answer, confidence);
	}
	if (nCorrect+nWrong > 0) {
		printf("TOTAL ACCURACY: %d%% out of %d tests.\n", nCorrect * 100/(nCorrect+nWrong), (nCorrect+nWrong));
	}

	printf("MATRIX CONFUSION :\n");
	printf("________________________________________________\n");
	printf("\t|");
	for(int i = 0; i < numberTrainingPersons; i++){
		printf("%s\t|", personNames.at(i).c_str());
	}
	printf("unknown|\n");
	printf("------------------------------------------------|\n");
	
	for(int i = 0; i < numberTestingPersons - 1; i++){
		printf("%s\t| ", personNames.at(i).c_str());
		for(int j = 0; j < numberTestingPersons; j++){
			printf("%d\t| ", confusionMat[i][j]);
		}
		printf("\n");
	}
	printf("unknown\t| ");
	for(int j = 0; j < numberTestingPersons; j++){
		printf("%d\t| ", confusionMat[numberTestingPersons - 1][j]);
	}
	printf("\n------------------------------------------------\n");
}

void recognize(Mat frame){
	int nTestFaces  = 0;         // the number of test images
	CvMat * trainPersonNumMat = 0;  // the person numbers during training
	float * projectedTestFace = 0;
	float confidence;

	vector<Rect> faces;
    Mat frame_gray;
    Mat crop;
    Mat res;
    string text;
    stringstream sstm;

    cvtColor(frame, frame_gray, COLOR_BGR2GRAY);
    equalizeHist(frame_gray, frame_gray);

    // Detect faces
    face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30, 30));

    // Set Region of Interest
    Rect roi_c;

	// load the saved training data
	if( !loadTrainingData( &trainPersonNumMat ) ) return;
	personNames.push_back("unknown");

	// project the test images onto the PCA subspace
	projectedTestFace = (float *)cvAlloc( numberEigenValues*sizeof(float) );

    // Iterate through all current elements (detected faces)
    for (int i = 0; i < faces.size(); i++){
        roi_c.x = faces[i].x;
        roi_c.y = faces[i].y;
        roi_c.width = (faces[i].width);
        roi_c.height = (faces[i].height);

        crop = frame_gray(roi_c);
        resize(crop, res, Size(128, 128), 0, 0, INTER_LINEAR); // This will be needed later while saving images

        int indexNearest, nearest;

        // project the test image onto the PCA subspace
		cvEigenDecomposite(
			new IplImage(res),
			numberEigenValues,
			eigenVectors,
			0, 0,
			avgTrainImage,
			projectedTestFace);

		indexNearest = findNearestNeighbor(projectedTestFace, &confidence);

		if(confidence > seuilConfidence){
			nearest  = trainPersonNumMat->data.i[indexNearest];
		}else{
			nearest = numberTrainingPersons + 1;
		}

        Point pt1(faces[i].x, faces[i].y); // Display detected faces on main window 
        Point pt2((faces[i].x + faces[i].height), (faces[i].y + faces[i].width));
        rectangle(frame, pt1, pt2, Scalar(0, 255, 0), 2, 8, 0);
        putText(frame, personNames.at(nearest - 1), Point(faces[i].x, faces[i].y - 10), FONT_HERSHEY_PLAIN, 2.0, CV_RGB(0, 255, 0), 2.0);
    }
    imshow("recognize", frame);
    waitKey();
}

IplImage* convertImageToGreyscale(const IplImage *imageSrc)
{
	IplImage *imageGrey;
	if (imageSrc->nChannels == 3) {
		imageGrey = cvCreateImage( cvGetSize(imageSrc), IPL_DEPTH_8U, 1 );
		cvCvtColor( imageSrc, imageGrey, CV_BGR2GRAY );
	}
	else {
		imageGrey = cvCloneImage(imageSrc);
	}
	return imageGrey;
}

IplImage* resizeImage(const IplImage *origImg, int newWidth, int newHeight)
{
	IplImage *outImg = 0;
	int origWidth;
	int origHeight;
	if (origImg) {
		origWidth = origImg->width;
		origHeight = origImg->height;
	}
	if (newWidth <= 0 || newHeight <= 0 || origImg == 0 || origWidth <= 0 || origHeight <= 0) {
		printf("ERROR in resizeImage: Bad desired image size of %dx%d\n.", newWidth, newHeight);
		exit(1);
	}

	// Scale the image to the new dimensions, even if the aspect ratio will be changed.
	outImg = cvCreateImage(cvSize(newWidth, newHeight), origImg->depth, origImg->nChannels);
	if (newWidth > origImg->width && newHeight > origImg->height) {
		// Make the image larger
		cvResetImageROI((IplImage*)origImg);
		cvResize(origImg, outImg, CV_INTER_LINEAR);	
	}
	else {
		// Make the image smaller
		cvResetImageROI((IplImage*)origImg);
		cvResize(origImg, outImg, CV_INTER_AREA);	
	}

	return outImg;
}

// Returns a new image that is a cropped version of the original image. 
IplImage* cropImage(const IplImage *img, const CvRect region)
{
	IplImage *imageTmp;
	IplImage *imageRGB;
	CvSize size;
	size.height = img->height;
	size.width = img->width;

	if (img->depth != IPL_DEPTH_8U) {
		printf("ERROR in cropImage: Unknown image depth of %d given in cropImage() instead of 8 bits per pixel.\n", img->depth);
		exit(1);
	}

	// First create a new (color or greyscale) IPL Image and copy contents of img into it.
	imageTmp = cvCreateImage(size, IPL_DEPTH_8U, img->nChannels);
	cvCopy(img, imageTmp, NULL);

	// Create a new image of the detected region
	// Set region of interest to that surrounding the face
	cvSetImageROI(imageTmp, region);
	// Copy region of interest (i.e. face) into a new iplImage (imageRGB) and return it
	size.width = region.width;
	size.height = region.height;
	imageRGB = cvCreateImage(size, IPL_DEPTH_8U, img->nChannels);
	cvCopy(imageTmp, imageRGB, NULL);	// Copy just the region.

    cvReleaseImage( &imageTmp );
	return imageRGB;		
}

// Get an 8-bit equivalent of the 32-bit Float image.
IplImage* convertFloatImageToUcharImage(const IplImage *srcImg)
{
	IplImage *dstImg = 0;
	if ((srcImg) && (srcImg->width > 0 && srcImg->height > 0)) {

		// Spread the 32bit floating point pixels to fit within 8bit pixel range.
		double minVal, maxVal;
		cvMinMaxLoc(srcImg, &minVal, &maxVal);

		// Deal with NaN and extreme values, since the DFT seems to give some NaN results.
		if (cvIsNaN(minVal) || minVal < -1e30)
			minVal = -1e30;
		if (cvIsNaN(maxVal) || maxVal > 1e30)
			maxVal = 1e30;
		if (maxVal-minVal == 0.0f)
			maxVal = minVal + 0.001;	// remove potential divide by zero errors.

		// Convert the format
		dstImg = cvCreateImage(cvSize(srcImg->width, srcImg->height), 8, 1);
		cvConvertScale(srcImg, dstImg, 255.0 / (maxVal - minVal), - minVal * 255.0 / (maxVal-minVal));
	}
	return dstImg;
}

// Store a greyscale floating-point CvMat image into a BMP/JPG/GIF/PNG image,
// since cvSaveImage() can only handle 8bit images (not 32bit float images).
void saveFloatImage(const char *filename, const IplImage *srcImg){
	IplImage *byteImg = convertFloatImageToUcharImage(srcImg);
	cvSaveImage(filename, byteImg);
	cvReleaseImage(&byteImg);
}

// Perform face detection on the input image, using the given Haar cascade classifier.
// Returns a rectangle for the detected region in the given image.
CvRect detectFaceInImage(const IplImage *inputImg, const CvHaarClassifierCascade* cascade ){
	const CvSize minFeatureSize = cvSize(20, 20);
	const int flags = CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_DO_ROUGH_SEARCH;	// Only search for 1 face.
	const float search_scale_factor = 1.1f;
	IplImage *detectImg;
	IplImage *greyImg = 0;
	CvMemStorage* storage;
	CvRect rc;
	double t;
	CvSeq* rects;

	storage = cvCreateMemStorage(0);
	cvClearMemStorage( storage );

	// If the image is color, use a greyscale copy of the image.
	detectImg = (IplImage*)inputImg;	// Assume the input image is to be used.
	if (inputImg->nChannels > 1) {
		greyImg = cvCreateImage(cvSize(inputImg->width, inputImg->height), IPL_DEPTH_8U, 1 );
		cvCvtColor( inputImg, greyImg, CV_BGR2GRAY );
		detectImg = greyImg;	// Use the greyscale version as the input.
	}

	// Detect all the faces.
	t = (double)cvGetTickCount();
	rects = cvHaarDetectObjects( detectImg, (CvHaarClassifierCascade*)cascade, storage,
				search_scale_factor, 3, flags, minFeatureSize );
	t = (double)cvGetTickCount() - t;
	printf("[Face Detection took %d ms and found %d objects]\n", cvRound( t/((double)cvGetTickFrequency()*1000.0) ), rects->total );

	// Get the first detected face (the biggest).
	if (rects->total > 0) {
        rc = *(CvRect*)cvGetSeqElem( rects, 0 );
    }
	else
		rc = cvRect(-1,-1,-1,-1);	// Couldn't find the face.

	if (greyImg)
		cvReleaseImage( &greyImg );
	cvReleaseMemStorage( &storage );

	return rc;	// Return the biggest face found, or (-1,-1,-1,-1).
}